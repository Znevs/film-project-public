﻿package com.znevs.filmproject

import coil.Coil
import coil.ImageLoader
import com.znevs.filmproject.di.component.DaggerAppComponent
import com.znevs.filmproject.presentation.ui.global.base.BaseApplication
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class App : BaseApplication() {

    override fun onCreate() {
        super.onCreate()
        Coil.setImageLoader(
            imageLoader = ImageLoader.Builder(applicationContext)
                .availableMemoryPercentage(0.25)
                .crossfade(true)
                .crossfade(500)
                .build()
        )
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(this)
    }
}
