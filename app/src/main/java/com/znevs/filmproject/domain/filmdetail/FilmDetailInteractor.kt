﻿package com.znevs.filmproject.domain.filmdetail

import com.znevs.filmproject.domain.global.models.FilmDetailDomainModel
import com.znevs.filmproject.domain.global.repositories.FilmsRepository
import javax.inject.Inject

class FilmDetailInteractor @Inject constructor(
    private val filmsRepository: FilmsRepository
) {
    suspend fun getFilmDetail(id: Long): FilmDetailDomainModel {
        return filmsRepository.getFilmDetail(id)
    }
}