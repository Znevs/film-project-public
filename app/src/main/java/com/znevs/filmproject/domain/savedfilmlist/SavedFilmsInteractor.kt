﻿package com.znevs.filmproject.domain.savedfilmlist

import com.znevs.filmproject.domain.global.models.FilmDetailDomainModel
import com.znevs.filmproject.domain.global.models.FilmDomainModel
import com.znevs.filmproject.domain.global.repositories.FilmsRepository
import javax.inject.Inject

class SavedFilmsInteractor @Inject constructor(
    private val filmsRepository: FilmsRepository
) {

    suspend fun getSavedFilms(): List<FilmDomainModel> {
        return filmsRepository.getSavedFilms()
    }

    suspend fun deleteSaveFilm(id: Long): Boolean {
        return filmsRepository.deleteSaveFilm(id)
    }

    suspend fun getFilmDetail(id: Long): FilmDetailDomainModel {
        return filmsRepository.getFilmDetail(id)
    }
}