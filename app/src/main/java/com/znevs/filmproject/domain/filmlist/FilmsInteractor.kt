package com.znevs.filmproject.domain.filmlist

import com.znevs.filmproject.domain.global.models.FilmDetailDomainModel
import com.znevs.filmproject.domain.global.models.FilmDomainModel
import com.znevs.filmproject.domain.global.repositories.FilmsRepository
import javax.inject.Inject

class FilmsInteractor @Inject constructor(
    private val filmsRepository: FilmsRepository
) {

    suspend fun getFilms(): List<FilmDomainModel> {
        return filmsRepository.getFilms()
    }

    suspend fun saveFilm(model: FilmDomainModel): Boolean {
        return filmsRepository.saveFilm(model)
    }

    suspend fun deleteSaveFilm(id: Long): Boolean {
        return filmsRepository.deleteSaveFilm(id)
    }

    suspend fun isFilmSaved(id: Long): Boolean {
        return filmsRepository.isFilmSaved(id)
    }

    suspend fun getFilmDetail(id: Long): FilmDetailDomainModel {
        return filmsRepository.getFilmDetail(id)
    }
}