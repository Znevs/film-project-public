﻿package com.znevs.filmproject.domain.global.repositories

import com.znevs.filmproject.domain.global.models.FilmDetailDomainModel
import com.znevs.filmproject.domain.global.models.FilmDomainModel

interface FilmsRepository {
    suspend fun getFilms(): List<FilmDomainModel>
    suspend fun getSavedFilms(): List<FilmDomainModel>
    suspend fun saveFilm(model: FilmDomainModel): Boolean
    suspend fun deleteSaveFilm(id: Long): Boolean
    suspend fun isFilmSaved(id: Long): Boolean
    suspend fun getFilmDetail(id: Long): FilmDetailDomainModel
}