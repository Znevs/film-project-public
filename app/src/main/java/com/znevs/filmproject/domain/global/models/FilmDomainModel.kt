﻿package com.znevs.filmproject.domain.global.models

data class FilmDomainModel(
    val filmId: Long,
    val nameRu: String,
    val year: String,
    val rating: Double,
    val posterUrlPreview: String,
    val isSaved: Boolean
)