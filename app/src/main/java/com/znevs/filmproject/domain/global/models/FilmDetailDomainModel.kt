﻿package com.znevs.filmproject.domain.global.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FilmDetailDomainModel(
    val filmId: Int,
    val countries: String,
    val description: String,
    val facts: String,
    val filmLength: String,
    val images: List<String>,
    val rating: Double,
    val genres: List<String>,
    val ratingAgeLimits: Int,
    val nameRu: String,
    val posterUrl: String,
    val posterUrlPreview: String,
    val premiereWorld: String,
    val slogan: String,
    val year: String
) : Parcelable