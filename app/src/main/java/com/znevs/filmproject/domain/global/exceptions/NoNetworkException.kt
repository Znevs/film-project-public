﻿package com.znevs.filmproject.domain.global.exceptions

import java.io.IOException

class NoNetworkException(
    message: String? = "",
    cause: Throwable? = null
) : IOException(message, cause)