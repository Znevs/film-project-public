﻿package com.znevs.filmproject.di.module

import com.znevs.filmproject.di.module.main.MainFragmentBuildersModule
import com.znevs.filmproject.di.module.main.MainModule
import com.znevs.filmproject.di.module.main.MainViewModelModule
import com.znevs.filmproject.di.scope.ActivityScope
import com.znevs.filmproject.presentation.ui.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector(modules = [MainFragmentBuildersModule::class, MainViewModelModule::class, MainModule::class])
    @ActivityScope
    internal abstract fun contributeMainActivity(): MainActivity
}