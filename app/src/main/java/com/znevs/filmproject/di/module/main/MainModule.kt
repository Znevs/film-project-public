﻿package com.znevs.filmproject.di.module.main

import android.content.Context
import androidx.room.Room
import com.znevs.filmproject.data.database.FilmDatabase
import com.znevs.filmproject.data.repositories.FilmsRepositoryImpl
import com.znevs.filmproject.domain.global.repositories.FilmsRepository
import dagger.Module
import dagger.Provides

@Module
class MainModule {

    @Provides
    fun provideSavedFilmsDatabase(context: Context): FilmDatabase {
        return Room.databaseBuilder(context, FilmDatabase::class.java, "database").build()
    }

    @Provides
    fun provideFilmsRepository(repository: FilmsRepositoryImpl): FilmsRepository {
        return repository
    }
}