﻿package com.znevs.filmproject.di.module

import com.znevs.filmproject.presentation.ui.global.navigation.PlaneRouter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import javax.inject.Singleton


@Module
class NavigationModule {

    private val cicerone: Cicerone<PlaneRouter> = Cicerone.create(PlaneRouter())

    @Provides
    @Singleton
    fun provideRouter(): PlaneRouter = cicerone.router

    @Provides
    @Singleton
    fun provideNavigatorHolder(): NavigatorHolder = cicerone.navigatorHolder
}
