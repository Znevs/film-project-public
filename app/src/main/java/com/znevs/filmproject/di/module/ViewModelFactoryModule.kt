﻿package com.znevs.filmproject.di.module

import androidx.lifecycle.ViewModelProvider
import com.znevs.filmproject.presentation.ui.global.ViewModelProviderFactory

import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory
}