﻿package com.znevs.filmproject.di.component

import com.znevs.filmproject.App
import com.znevs.filmproject.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
        AppModule::class,
        ActivityBuildersModule::class,
        NetworkModule::class,
        NavigationModule::class,
        ViewModelFactoryModule::class]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Factory
    interface Builder : AndroidInjector.Factory<App> {
        override fun create(@BindsInstance instance: App): AppComponent
    }
}