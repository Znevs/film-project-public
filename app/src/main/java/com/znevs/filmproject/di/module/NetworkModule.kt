﻿package com.znevs.filmproject.di.module

import com.znevs.filmproject.BuildConfig
import com.znevs.filmproject.data.network.ApiService
import com.znevs.filmproject.data.network.interceptors.HeaderInterceptor
import com.znevs.filmproject.data.network.interceptors.NetworkCheckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
object NetworkModule {

    private const val API_VERSION = "api"

    @Provides
    @Singleton
    fun provideOkHttp(
        networkCheckInterceptor: NetworkCheckInterceptor,
        headerInterceptor: HeaderInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().apply {
            level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                else HttpLoggingInterceptor.Level.NONE
        })
        .addInterceptor(headerInterceptor)
        .addInterceptor(networkCheckInterceptor)
        .build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl("${BuildConfig.API_URL}/$API_VERSION/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()

    @Provides
    @Singleton
    fun provideService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)
}
