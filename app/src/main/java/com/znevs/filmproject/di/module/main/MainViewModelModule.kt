﻿package com.znevs.filmproject.di.module.main

import androidx.lifecycle.ViewModel
import com.znevs.filmproject.di.scope.FragmentScope
import com.znevs.filmproject.di.viewmodel.ViewModelKey
import com.znevs.filmproject.presentation.ui.filmdetail.FilmDetailViewModel
import com.znevs.filmproject.presentation.ui.filmlist.FilmsViewModel
import com.znevs.filmproject.presentation.ui.savedfilmlist.SavedFilmsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FilmsViewModel::class)
    abstract fun bindFilmsViewModel(viewModel: FilmsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SavedFilmsViewModel::class)
    abstract fun bindSavedFilmsViewModel(viewModel: SavedFilmsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FilmDetailViewModel::class)
    abstract fun bindFilmDetailViewModel(viewModel: FilmDetailViewModel): ViewModel
}