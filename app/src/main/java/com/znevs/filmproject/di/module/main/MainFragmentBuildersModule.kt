﻿package com.znevs.filmproject.di.module.main

import com.znevs.filmproject.presentation.ui.flowmain.MainFlowFragment
import com.znevs.filmproject.presentation.ui.filmdetail.FilmDetailFragment
import com.znevs.filmproject.presentation.ui.filmlist.FilmsFragment
import com.znevs.filmproject.presentation.ui.savedfilmlist.SavedFilmsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeFilmsFragment(): FilmsFragment

    @ContributesAndroidInjector
    abstract fun contributeSavedFilmsFragment(): SavedFilmsFragment

    @ContributesAndroidInjector
    abstract fun contributeMainFlowFragment(): MainFlowFragment

    @ContributesAndroidInjector
    abstract fun contributeFilmDetailFragment(): FilmDetailFragment
}