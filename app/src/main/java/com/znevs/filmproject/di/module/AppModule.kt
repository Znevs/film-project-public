﻿package com.znevs.filmproject.di.module

import android.content.Context
import com.znevs.filmproject.App
import dagger.Binds
import dagger.Module
import javax.inject.Singleton


@Module
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun application(app: App): Context
}