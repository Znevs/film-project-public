﻿package com.znevs.filmproject.data.network.response

import com.znevs.filmproject.data.models.filmframes.FrameDataModel

data class FilmFramesResponse(
    val frames: List<FrameDataModel>
)