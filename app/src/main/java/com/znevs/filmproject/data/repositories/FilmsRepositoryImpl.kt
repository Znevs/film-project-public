﻿package com.znevs.filmproject.data.repositories

import android.util.Log
import com.znevs.filmproject.data.database.FilmDatabase
import com.znevs.filmproject.data.database.entities.toDatabase
import com.znevs.filmproject.data.database.entities.toDomain
import com.znevs.filmproject.data.models.filmdetail.toDomain
import com.znevs.filmproject.data.models.toDomain
import com.znevs.filmproject.data.network.ApiService
import com.znevs.filmproject.domain.global.models.FilmDetailDomainModel
import com.znevs.filmproject.domain.global.models.FilmDomainModel
import com.znevs.filmproject.domain.global.repositories.FilmsRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import javax.inject.Inject

class FilmsRepositoryImpl @Inject constructor(
    private val api: ApiService,
    private val db: FilmDatabase
) : FilmsRepository {

    override suspend fun getFilms(): List<FilmDomainModel> {
        return api.getFilms().films.map { e ->
            val isFilmSaved = db.getFilmsDao().isFilmSaved(e.filmId)
            e.toDomain(isFilmSaved)
        }
    }

    override suspend fun getSavedFilms(): List<FilmDomainModel> {
        return db.getFilmsDao().getFilms()?.map { e ->
            e.toDomain()
        } ?: run {
            emptyList()
        }
    }

    override suspend fun saveFilm(model: FilmDomainModel): Boolean {
        return try {
            db.getFilmsDao().saveFilm(model.toDatabase())
            true
        } catch (e: Exception) {
            false
        }
    }

    override suspend fun deleteSaveFilm(id: Long): Boolean {
        return try {
            db.getFilmsDao().deleteFilm(id)
            true
        } catch (e: Exception) {
            false
        }
    }

    override suspend fun isFilmSaved(id: Long): Boolean {
        return try {
            db.getFilmsDao().isFilmSaved(id)
        } catch (e: Exception) {
            false
        }
    }

    override suspend fun getFilmDetail(id: Long): FilmDetailDomainModel = coroutineScope {
        val deferredFilmFrames = async {
            api.getFilmFrames(id).frames
        }

        val deferredFilmDetail = async {
            api.getFilmDetail(id).data
        }

        val filmDetail = deferredFilmDetail.await()
        val filmFrames = deferredFilmFrames.await()

        filmDetail.toDomain(filmFrames)
    }
}