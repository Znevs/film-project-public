﻿package com.znevs.filmproject.data.models.filmdetail

data class Poster(
    val height: Int,
    val language: String,
    val url: String,
    val width: Int
)