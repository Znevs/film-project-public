﻿package com.znevs.filmproject.data.models.filmdetail

data class Review(
    val ratingGoodReview: String,
    val ratingGoodReviewVoteCount: Int,
    val reviewsCount: Int
)