﻿package com.znevs.filmproject.data.models.filmdetail

data class Images(
    val backdrops: List<Backdrop>,
    val posters: List<Poster>
)