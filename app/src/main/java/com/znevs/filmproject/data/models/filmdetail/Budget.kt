﻿package com.znevs.filmproject.data.models.filmdetail

data class Budget(
    val budget: String,
    val grossRu: Int,
    val grossUsa: Int,
    val grossWorld: Int,
    val marketing: Int
)