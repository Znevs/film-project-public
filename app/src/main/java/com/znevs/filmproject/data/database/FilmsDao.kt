﻿package com.znevs.filmproject.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.znevs.filmproject.data.database.entities.SavedFilmEntity

@Dao
interface FilmsDao {

    @Query("SELECT * FROM saved_films")
    suspend fun getFilms(): List<SavedFilmEntity>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveFilm(film: SavedFilmEntity)

    @Query("SELECT EXISTS(SELECT * FROM saved_films WHERE id = :id)")
    suspend fun isFilmSaved(id: Long): Boolean

    @Query("DELETE FROM saved_films WHERE id = :id")
    suspend fun deleteFilm(id: Long)
}

