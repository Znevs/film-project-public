﻿package com.znevs.filmproject.data.network

import com.znevs.filmproject.data.network.response.FilmDetailResponse
import com.znevs.filmproject.data.network.response.FilmFramesResponse
import com.znevs.filmproject.data.network.response.FilmsResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("v2.2/films/top")
    suspend fun getFilms(
        @Query("type") type: String = "TOP_250_BEST_FILMS",
        @Query("page") page: Int = 1
    ): FilmsResponse

    @GET("v2.1/films/{id}")
    suspend fun getFilmDetail(@Path("id") id: Long): FilmDetailResponse

    @GET("v2.1/films/{id}/frames")
    suspend fun getFilmFrames(@Path("id") id: Long): FilmFramesResponse
}