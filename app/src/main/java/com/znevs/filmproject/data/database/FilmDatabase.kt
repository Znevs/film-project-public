﻿package com.znevs.filmproject.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.znevs.filmproject.data.database.entities.SavedFilmEntity

@Database(entities = [SavedFilmEntity::class], version = 1)
@TypeConverters(Converters::class)
abstract class FilmDatabase : RoomDatabase() {
    abstract fun getFilmsDao(): FilmsDao
}