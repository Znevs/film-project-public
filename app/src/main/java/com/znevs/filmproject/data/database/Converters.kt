﻿package com.znevs.filmproject.data.database

import androidx.room.TypeConverter

object Converters {
    @TypeConverter
    @JvmStatic
    fun fromList(list: List<String>?): String {
        if (list == null || list.isEmpty()) {
            return ""
        }
        return list.joinToString(separator = ", ")
    }


    @TypeConverter
    @JvmStatic
    fun toString(data: String): List<String>? {
        if (data.isBlank()) {
            return emptyList()
        }

        return data.split(", ").map { it.trim() }
    }
}