﻿package com.znevs.filmproject.data.network.response

import com.znevs.filmproject.data.models.filmdetail.FilmDetailDataModel

data class FilmDetailResponse(
    val data: FilmDetailDataModel
)