﻿package com.znevs.filmproject.data.models.filmframes

data class FrameDataModel(
    val image: String,
    val preview: String
)