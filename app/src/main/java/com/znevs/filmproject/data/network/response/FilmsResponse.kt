﻿package com.znevs.filmproject.data.network.response

import com.znevs.filmproject.data.models.FilmDataModel

data class FilmsResponse(
    val pagesCount: Int,
    val films: List<FilmDataModel>
)