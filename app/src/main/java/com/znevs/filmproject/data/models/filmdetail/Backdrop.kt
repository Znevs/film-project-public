﻿package com.znevs.filmproject.data.models.filmdetail

data class Backdrop(
    val height: Int,
    val language: String,
    val url: String,
    val width: Int
)