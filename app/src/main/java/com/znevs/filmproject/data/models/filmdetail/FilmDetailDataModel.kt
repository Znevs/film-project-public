﻿package com.znevs.filmproject.data.models.filmdetail

import com.znevs.filmproject.data.models.filmframes.FrameDataModel
import com.znevs.filmproject.domain.global.models.FilmDetailDomainModel

data class FilmDetailDataModel(
    val filmId: Int,
    val countries: List<Country>?,
    val description: String?,
    val distributorRelease: String?,
    val distributors: String?,
    val facts: List<String>?,
    val filmLength: String?,
    val genres: List<Genre>?,
    val nameEn: String?,
    val nameRu: String?,
    val posterUrl: String?,
    val posterUrlPreview: String?,
    val premiereBluRay: String?,
    val premiereDigital: String?,
    val premiereDvd: String?,
    val premiereRu: String?,
    val premiereWorld: String?,
    val premiereWorldCountry: String?,
    val images: Images?,
    val ratingAgeLimits: Int?,
    val ratingMpaa: String?,
    val rating: Rating?,
    val seasons: List<Season>?,
    val slogan: String?,
    val type: String?,
    val webUrl: String?,
    val year: String?
)

fun FilmDetailDataModel.toDomain(frames: List<FrameDataModel>): FilmDetailDomainModel {
    return FilmDetailDomainModel(
        filmId = this.filmId,
        countries = this.countries?.joinToString(separator = ", ") { it.country } ?: "",
        description = this.description ?: "",
        facts = this.facts?.mapIndexed { index, s -> "${index + 1}. $s" }?.joinToString(separator = "\n\n") ?: "",
        filmLength = this.filmLength ?: "0:00",
        genres = this.genres?.filter { it.genre.isNotEmpty() }?.map { it.genre } ?: emptyList(),
        ratingAgeLimits = this.ratingAgeLimits ?: 0,
        nameRu = this.nameRu ?: "",
        posterUrl = this.posterUrl ?: "",
        posterUrlPreview = this.posterUrlPreview ?: "",
        premiereWorld = this.premiereWorld ?: "",
        slogan = this.slogan ?: "",
        year = this.year ?: "????",
        images = frames.map { it.preview },
        rating = this.rating?.rating ?: 0.0
    )
}