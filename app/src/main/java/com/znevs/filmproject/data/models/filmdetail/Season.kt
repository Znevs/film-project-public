﻿package com.znevs.filmproject.data.models.filmdetail

data class Season(
    val episodes: List<Episode>,
    val number: Int
)