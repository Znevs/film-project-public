﻿package com.znevs.filmproject.data.models

import com.znevs.filmproject.domain.global.models.FilmDomainModel

data class FilmDataModel(
    val filmId: Long,
    val nameRu: String?,
    val nameEn: String?,
    val year: String?,
    val rating: Double?,
    val posterUrl: String?,
    val posterUrlPreview: String?,
)

fun FilmDataModel.toDomain(isSaved: Boolean) = FilmDomainModel(
    filmId = this.filmId,
    nameRu = this.nameRu ?: "",
    year = this.year ?: "????",
    rating = this.rating ?: 0.0,
    posterUrlPreview = this.posterUrlPreview ?: "",
    isSaved = isSaved
)