﻿package com.znevs.filmproject.data.models.filmdetail

data class Country(
    val country: String
)