﻿package com.znevs.filmproject.data.models.filmdetail

data class ExternalId(
    val imdbId: String
)