package com.znevs.filmproject.data.network.interceptors

import com.znevs.filmproject.data.network.NetworkChecker
import com.znevs.filmproject.domain.global.exceptions.NoNetworkException
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

class NetworkCheckInterceptor @Inject constructor(private val networkChecker: NetworkChecker) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        if (!networkChecker.isConnected) {
            throw NoNetworkException("No network connection")
        }
        return try {
            chain.proceed(requestBuilder.build())
        } catch (e: SocketTimeoutException) {
            throw NoNetworkException("Timeout")
        } catch (e: UnknownHostException) {
            throw NoNetworkException("Unknown host")
        }
    }
}
