﻿package com.znevs.filmproject.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.znevs.filmproject.domain.global.models.FilmDomainModel

@Entity(tableName = "saved_films")
data class SavedFilmEntity(
    @PrimaryKey var id: Long = 0,
    val nameRu: String,
    val year: String,
    val rating: Double,
    val posterUrlPreview: String
)

fun SavedFilmEntity.toDomain() = FilmDomainModel(
    filmId = this.id,
    nameRu = this.nameRu,
    year = this.year,
    rating = this.rating,
    posterUrlPreview = this.posterUrlPreview,
    isSaved = true
)

fun FilmDomainModel.toDatabase() = SavedFilmEntity(
    id = this.filmId,
    nameRu = this.nameRu,
    year = this.year,
    rating = this.rating,
    posterUrlPreview = this.posterUrlPreview
)