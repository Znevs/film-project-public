﻿package com.znevs.filmproject.presentation.ui.filmdetail

import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipDrawable
import com.znevs.filmproject.R
import com.znevs.filmproject.domain.global.models.FilmDetailDomainModel
import com.znevs.filmproject.presentation.ui.global.ViewModelProviderFactory
import com.znevs.filmproject.presentation.ui.global.adapters.FilmsImageRecyclerAdapter
import com.znevs.filmproject.presentation.ui.global.base.BaseFragment
import com.znevs.filmproject.presentation.utils.extension.addLoadError
import com.znevs.filmproject.presentation.utils.extension.injectViewModel
import com.znevs.filmproject.presentation.utils.extension.withArguments
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.fragment_film_detail.*
import kotlinx.android.synthetic.main.toolbar_top.*
import javax.inject.Inject


@Parcelize
data class FilmDetailScreenData(
    val film: FilmDetailDomainModel
) : Parcelable

class FilmDetailFragment : BaseFragment(R.layout.fragment_film_detail) {

    companion object {
        private const val ARG_DATA = "ARG_DATA"
        fun newInstance(data: FilmDetailScreenData): FilmDetailFragment = FilmDetailFragment()
            .withArguments {
                putParcelable(ARG_DATA, data)
            }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory
    private lateinit var viewModel: FilmDetailViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = injectViewModel(viewModelFactory)
        val imageAdapter = FilmsImageRecyclerAdapter()

        rv_item_film_image.apply {
            adapter = imageAdapter
            itemAnimator = null
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }

        viewModel.viewStates().observe(viewLifecycleOwner, { state ->
            when (state) {
                is FilmDetailViewState.Data -> {
                    val data = state.data
                    txt_toolbar_title.text = data.nameRu
                    img_item_film_poster.load(data.posterUrl) { addLoadError(img_item_film_poster) }
                    img_item_film_poster_preview.load(data.posterUrlPreview) { addLoadError(img_item_film_poster_preview) }

                    txt_item_film_title.text = data.nameRu
                    txt_item_film_year.text = data.year
                    txt_item_film_countries.isVisible = data.countries.isNotEmpty()
                    txt_item_film_countries.text = data.countries

                    img_item_film_rating.isVisible = data.rating > 0.0
                    txt_item_film_rating.text = if (data.rating == 0.0) getString(R.string.no) else data.rating.toString()
                    txt_item_film_age.text = data.ratingAgeLimits.toString()
                    txt_item_film_length.text = getFilmLengthFormat(data.filmLength)

                    rv_item_film_image.isVisible = data.images.isNotEmpty()
                    imageAdapter.submitList(data.images)

                    cg_item_film_genres.isVisible = data.genres.isNotEmpty()
                    for (genre in data.genres) {

                        val chip = Chip(cg_item_film_genres.context)
                        val chipDrawable = ChipDrawable.createFromAttributes(
                            cg_item_film_genres.context,
                            null,
                            0,
                            R.style.AppTheme_Chip
                        )
                        chip.setChipDrawable(chipDrawable)
                        chip.text = genre
                        cg_item_film_genres.addView(chip)
                    }

                    group_slogan.isVisible = data.slogan.isNotEmpty()
                    txt_item_film_slogan.text = data.slogan

                    group_facts.isVisible = data.facts.isNotEmpty()
                    txt_item_film_facts.text = data.facts

                    group_description.isVisible = data.description.isNotEmpty()
                    txt_item_film_description.text = data.description
                }
            }
        })

        img_toolbar_back.setOnClickListener {
            viewModel.processEvent(FilmDetailViewEvent.Back)
        }

        if (savedInstanceState == null) {
            val film = (getArgument(ARG_DATA) as FilmDetailScreenData).film
            viewModel.processEvent(FilmDetailViewEvent.SetFilmData(film))
        }
    }

    private fun getFilmLengthFormat(filmLength: String): String {
        filmLength.split(":").apply {
            return "${get(0)} ${getString(R.string.hours_short)} ${get(1)} ${getString(R.string.minutes_short)}"
        }
    }
}