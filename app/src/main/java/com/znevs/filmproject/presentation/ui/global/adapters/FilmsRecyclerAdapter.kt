﻿package com.znevs.filmproject.presentation.ui.global.adapters

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.*
import coil.load
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView
import com.znevs.filmproject.R
import com.znevs.filmproject.domain.global.models.FilmDomainModel
import com.znevs.filmproject.presentation.ui.global.adapters.base.AdapterListener
import com.znevs.filmproject.presentation.utils.extension.addLoadError
import kotlinx.android.synthetic.main.item_film.view.*


class FilmsRecyclerAdapter(
    private val openFilmClickListener: AdapterListener<FilmDomainModel>? = null,
    private val saveClickListener: AdapterListener<FilmDomainModel>? = null
) : ListAdapter<FilmDomainModel, FilmsRecyclerAdapter.ViewHolder>(FilmDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_film, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).filmId
    }

    fun deleteItem(position: Int) {
        val list = currentList.toMutableList()
        list.removeAt(position)
        submitList(list)
    }

    fun addItem(position: Int, model: FilmDomainModel) {
        val list = currentList.toMutableList()
        list.add(position, model)
        submitList(list)
    }

    fun saveFilm(position: Int) {
        val list = currentList.toMutableList()
        list[position] = getItem(position).copy(isSaved = true)
        submitList(list)
    }

    fun deleteSaveFilm(position: Int) {
        val list = currentList.toMutableList()
        list[position] = getItem(position).copy(isSaved = false)
        submitList(list)
    }

    class FilmDiffCallback : DiffUtil.ItemCallback<FilmDomainModel>() {
        override fun areItemsTheSame(oldItem: FilmDomainModel, newItem: FilmDomainModel): Boolean {
            return newItem.filmId == oldItem.filmId
        }

        override fun areContentsTheSame(oldItem: FilmDomainModel, newItem: FilmDomainModel): Boolean {
            return newItem == oldItem
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val poster: ImageView = view.img_item_film
        private val title: MaterialTextView = view.txt_item_film_title
        private val rating: MaterialTextView = view.txt_item_film_rating
        private val year: MaterialTextView = view.txt_item_film_year
        private val saveFilmBtn: MaterialButton = view.btn_save_film

        fun bind(item: FilmDomainModel) {
            saveFilmBtn.setOnClickListener {
                saveClickListener?.onProcess(itemView, adapterPosition, item)
            }

            itemView.setOnClickListener {
                openFilmClickListener?.onProcess(itemView, adapterPosition, item)
            }

            val saveBtnColor = if (item.isSaved) R.color.delete_from_save_color else R.color.accent_color
            val saveTxt = if (item.isSaved) R.string.delete_from_save else R.string.save

            saveFilmBtn.setText(saveTxt)
            saveFilmBtn.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(saveFilmBtn.context, saveBtnColor))

            title.text = item.nameRu

            poster.load(item.posterUrlPreview) {
                addLoadError(poster)
            }
            rating.text = item.rating.toString()
            year.text = item.year
        }
    }
}