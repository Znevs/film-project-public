package com.znevs.filmproject.presentation.ui.global.navigation

import android.util.ArrayMap
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.Screen


class PlaneRouter : Router() {

    private val resultListeners = ArrayMap<Int, (Any) -> Unit>()

    /**
     * Subscribe to the screen result.<br></br>
     * **Note:** only one listener can subscribe to a unique resultCode!<br></br>
     * You must call a **removeResultListener()** to avoid a memory leak.
     *
     * @param resultCode key for filter results
     * @param listener   result listener
     */
    fun setResultListener(resultCode: Int?, listener: (Any) -> Unit) {
        resultListeners[resultCode] = listener
    }

    /**
     * Unsubscribe from the screen result.
     *
     * @param resultCode key for filter results
     */
    fun removeResultListener(resultCode: Int?) {
        resultListeners.remove(resultCode)
    }

    /**
     * Send result data to subscriber.
     *
     * @param resultCode result data key
     * @param result     result data
     * @return TRUE if listener was notified and FALSE otherwise
     */
    fun sendResult(resultCode: Int?, result: Any): Boolean {
        val resultListener = resultListeners[resultCode]
        if (resultListener != null) {
            resultListener(result)
            return true
        }
        return false
    }

    /**
     * Return to the previous screen in the chain and send result data.
     *
     * @param resultCode result data key
     * @param result     result data
     */
    fun exitWithResult(resultCode: Int?, result: Any) {
        exit()
        sendResult(resultCode, result)
    }

    /**
     * Return to the previous screen in the chain and send result data.
     *
     * @param resultCode result data key
     * @param result     result data
     */
    fun backToWithResult(screen: Screen, resultCode: Int?, result: Any) {
        backTo(screen)
        sendResult(resultCode, result)
    }

    fun backToRook() = backTo(null)
}
