﻿package com.znevs.filmproject.presentation.ui.filmdetail

import com.znevs.filmproject.domain.global.models.FilmDetailDomainModel
import com.znevs.filmproject.presentation.ui.global.base.BaseViewModel
import com.znevs.filmproject.presentation.ui.global.navigation.PlaneRouter
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject


sealed class FilmDetailViewState {
    object Default : FilmDetailViewState()
    data class Data(val data: FilmDetailDomainModel) : FilmDetailViewState()
}

sealed class FilmDetailViewEvent {
    data class SetFilmData(val model: FilmDetailDomainModel) : FilmDetailViewEvent()
    object Back : FilmDetailViewEvent()
}

class FilmDetailViewModel @Inject constructor(
    private val router: PlaneRouter
) : BaseViewModel<FilmDetailViewState, FilmDetailViewEvent, Nothing>() {

    init {
        viewState = FilmDetailViewState.Default
    }

    override fun processEvent(viewEvent: FilmDetailViewEvent) {
        when (viewEvent) {
            is FilmDetailViewEvent.SetFilmData -> {
                viewState = FilmDetailViewState.Data(viewEvent.model)
            }
            is FilmDetailViewEvent.Back -> {
                router.exit()
            }
        }
    }
}