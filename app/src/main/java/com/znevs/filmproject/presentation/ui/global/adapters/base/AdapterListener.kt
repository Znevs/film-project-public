﻿package com.znevs.filmproject.presentation.ui.global.adapters.base

import android.view.View

interface AdapterListener<T> {
    fun onProcess(view: View, position: Int, model: T)
}