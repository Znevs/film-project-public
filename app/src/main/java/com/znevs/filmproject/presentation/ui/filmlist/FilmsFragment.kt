﻿package com.znevs.filmproject.presentation.ui.filmlist

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.znevs.filmproject.R
import com.znevs.filmproject.domain.global.models.FilmDomainModel
import com.znevs.filmproject.presentation.ui.global.ViewModelProviderFactory
import com.znevs.filmproject.presentation.ui.global.adapters.FilmsRecyclerAdapter
import com.znevs.filmproject.presentation.ui.global.adapters.base.AdapterListener
import com.znevs.filmproject.presentation.ui.global.base.BaseFragment
import com.znevs.filmproject.presentation.utils.extension.injectViewModel
import kotlinx.android.synthetic.main.fragment_film_list.*
import kotlinx.android.synthetic.main.view_error_refresh.*
import javax.inject.Inject


class FilmsFragment : BaseFragment(R.layout.fragment_film_list) {

    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory
    private lateinit var viewModel: FilmsViewModel
    private lateinit var filmsAdapter: FilmsRecyclerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = injectViewModel(viewModelFactory)

        sl_film_list.setOnRefreshListener {
            refreshFilms()
        }

        btn_error_refresh.setOnClickListener {
            refreshFilms()
        }

        filmsAdapter = FilmsRecyclerAdapter(
            saveClickListener = saveFilmClickListener,
            openFilmClickListener = filmClickListener
        ).apply {
            setHasStableIds(true)
        }

        rv_film_list.apply {
            adapter = filmsAdapter
            itemAnimator = null
            layoutManager = GridLayoutManager(context, 2)
        }

        viewModel.viewStates().observe(viewLifecycleOwner, { state ->
            layout_loading.isVisible = state is FilmsViewState.Loading

            sl_film_list.isVisible = state is FilmsViewState.LoadingSwipe || state is FilmsViewState.Data
            sl_film_list.isRefreshing = state is FilmsViewState.LoadingSwipe

            layout_error_refresh.isVisible = state is FilmsViewState.Error

            when (state) {
                is FilmsViewState.Error -> {
                    processError(state.errorType)
                }
                is FilmsViewState.Data -> {
                    filmsAdapter.submitList(state.data)
                }
            }
        })

        viewModel.viewEffects().observe(viewLifecycleOwner, { effect ->
            processEffect(effect)
        })

        if (savedInstanceState == null) {
            viewModel.processEvent(FilmsViewEvent.LoadFilms)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        rv_film_list.adapter = null
    }

    override fun onResume() {
        super.onResume()
        updateItems()
    }

    private fun updateItems() {
        val itemList = filmsAdapter.currentList
        val listSize = itemList.size
        val pairModelList = mutableListOf<Pair<FilmDomainModel, Int>>()

        for (i in 0 until listSize) {
            pairModelList.add(itemList[i] to i)
        }

        if (pairModelList.isNotEmpty()) {
            viewModel.processEvent(FilmsViewEvent.CheckFilmsIsSaved(pairModelList))
        }
    }

    private fun refreshFilms() {
        viewModel.processEvent(FilmsViewEvent.RefreshFilms)
    }

    private fun processError(errorType: FilmsErrorType) {
        when (errorType) {
            is FilmsErrorType.NoNetworkError -> {
                filmsAdapter.submitList(emptyList())
                img_error_refresh.setImageResource(R.drawable.ic_wifi_off)
                txt_error_refresh.setText(R.string.error_no_network)
            }
            is FilmsErrorType.UnknownError -> {
                img_error_refresh.setImageResource(R.drawable.ic_error)
                txt_error_refresh.setText(R.string.error_unknown)
            }
            is FilmsErrorType.EmptyDataError -> {
                img_error_refresh.setImageResource(R.drawable.ic_cloud_off)
                txt_error_refresh.setText(R.string.message_empty_data)
            }
        }
    }

    private fun processEffect(effect: FilmsViewEffect) {
        when (effect) {
            is FilmsViewEffect.SaveFailed -> {
                filmsAdapter.deleteSaveFilm(effect.position)
            }
            is FilmsViewEffect.DeleteSaveFailed -> {
                filmsAdapter.saveFilm(effect.position)
            }
            is FilmsViewEffect.UpdateFilmsModel -> {
                val list = filmsAdapter.currentList.toMutableList()
                for (pair in effect.pairList) {
                    list[pair.second] = pair.first
                }

                filmsAdapter.submitList(list)
            }
        }
    }

    private val saveFilmClickListener = object : AdapterListener<FilmDomainModel> {
        override fun onProcess(view: View, position: Int, model: FilmDomainModel) {
            if (model.isSaved) {
                viewModel.processEvent(FilmsViewEvent.DeleteSavedFilm(model.filmId, position))
                filmsAdapter.deleteSaveFilm(position)
            } else {
                viewModel.processEvent(FilmsViewEvent.SaveFilm(model, position))
                filmsAdapter.saveFilm(position)
            }
        }
    }

    private val filmClickListener = object : AdapterListener<FilmDomainModel> {
        override fun onProcess(view: View, position: Int, model: FilmDomainModel) {
            viewModel.processEvent(FilmsViewEvent.ShowFilmDetail(model.filmId))
        }
    }
}