﻿package com.znevs.filmproject.presentation.ui.savedfilmlist

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.znevs.filmproject.R
import com.znevs.filmproject.domain.global.models.FilmDomainModel
import com.znevs.filmproject.presentation.ui.global.ViewModelProviderFactory
import com.znevs.filmproject.presentation.ui.global.adapters.FilmsRecyclerAdapter
import com.znevs.filmproject.presentation.ui.global.adapters.base.AdapterListener
import com.znevs.filmproject.presentation.ui.global.base.BaseFragment
import com.znevs.filmproject.presentation.utils.extension.injectViewModel
import kotlinx.android.synthetic.main.fragment_film_list.*
import kotlinx.android.synthetic.main.view_error_refresh.*
import javax.inject.Inject

class SavedFilmsFragment : BaseFragment(R.layout.fragment_film_list) {

    @Inject
    lateinit var viewModelFactory: ViewModelProviderFactory
    private lateinit var viewModel: SavedFilmsViewModel
    private lateinit var filmsAdapter: FilmsRecyclerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = injectViewModel(viewModelFactory)

        sl_film_list.setOnRefreshListener {
            loadFilms()
        }

        btn_error_refresh.setOnClickListener {
            loadFilms()
        }

        filmsAdapter = FilmsRecyclerAdapter(
            saveClickListener = saveFilmClickListener,
            openFilmClickListener = filmClickListener
        ).apply {
            setHasStableIds(true)
        }

        rv_film_list.apply {
            adapter = filmsAdapter
            itemAnimator = DefaultItemAnimator()
            layoutManager = GridLayoutManager(context, 2)
        }

        viewModel.viewStates().observe(viewLifecycleOwner, { state ->
            layout_loading.isVisible = state is SavedFilmsViewState.Loading

            sl_film_list.isVisible = state is SavedFilmsViewState.LoadingSwipe || state is SavedFilmsViewState.Data
            sl_film_list.isRefreshing = state is SavedFilmsViewState.LoadingSwipe

            layout_error_refresh.isVisible = state is SavedFilmsViewState.Error

            when (state) {
                is SavedFilmsViewState.Error -> {
                    processError(state.errorType)
                }
                is SavedFilmsViewState.Data -> {
                    filmsAdapter.submitList(state.data)
                }
            }
        })

        viewModel.viewEffects().observe(viewLifecycleOwner, { effect ->
            processEffect(effect)
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        rv_film_list.adapter = null
    }

    override fun onResume() {
        super.onResume()
        loadFilms()
    }

    private fun loadFilms() {
        viewModel.processEvent(SavedFilmsViewEvent.LoadFilms)
    }

    private fun processError(errorType: SavedFilmsErrorType) {
        when (errorType) {
            is SavedFilmsErrorType.NotSavedFilmsError -> {
                filmsAdapter.submitList(emptyList())
                img_error_refresh.setImageResource(R.drawable.ic_save)
                txt_error_refresh.setText(R.string.message_empty_saved_films)
            }
            is SavedFilmsErrorType.UnknownError -> {
                img_error_refresh.setImageResource(R.drawable.ic_cloud_off)
                txt_error_refresh.setText(R.string.error_unknown)
            }
        }
    }

    private fun processEffect(effect: SavedFilmsViewEffect) {
        when (effect) {
            is SavedFilmsViewEffect.DeleteFromSavedFailed -> {
                filmsAdapter.addItem(effect.position, effect.model)
            }
        }
    }

    private val saveFilmClickListener = object : AdapterListener<FilmDomainModel> {
        override fun onProcess(view: View, position: Int, model: FilmDomainModel) {
            viewModel.processEvent(SavedFilmsViewEvent.DeleteFromSavedFilm(model, position))
            filmsAdapter.deleteItem(position)
        }
    }

    private val filmClickListener = object : AdapterListener<FilmDomainModel> {
        override fun onProcess(view: View, position: Int, model: FilmDomainModel) {
            viewModel.processEvent(SavedFilmsViewEvent.ShowFilmDetail(model.filmId))
        }
    }
}