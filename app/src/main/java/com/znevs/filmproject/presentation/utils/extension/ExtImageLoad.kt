﻿package com.znevs.filmproject.presentation.utils.extension

import android.widget.ImageView
import coil.request.ImageRequest
import com.znevs.filmproject.R

infix fun ImageRequest.Builder.addLoadError(imageView: ImageView): ImageRequest.Builder {
    return target(
        onSuccess = {
            imageView.setImageDrawable(it)
            imageView.scaleType = ImageView.ScaleType.FIT_CENTER
        },
        onError = {
            imageView.scaleType = ImageView.ScaleType.CENTER
            imageView.setImageResource(R.drawable.ic_broken_image)
        },
        onStart = {
            imageView.scaleType = ImageView.ScaleType.CENTER
            imageView.setImageResource(R.drawable.ic_photo_camera)
        }
    )
}

