﻿package com.znevs.filmproject.presentation.ui.global.base

import android.os.Parcelable
import androidx.annotation.LayoutRes
import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment {
    constructor() : super()
    constructor(@LayoutRes contentLayoutId: Int) : super(contentLayoutId)

    protected fun <Data : Parcelable> getArgument(key: String): Data {
        arguments?.let {
            if (it.containsKey(key)) {
                it.getParcelable<Data>(key)?.let { data ->
                    return data
                }
            }
        }
        throw IllegalStateException("Not found data with key $key")
    }
}