﻿package com.znevs.filmproject.presentation.ui.filmlist

import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.viewModelScope
import com.znevs.filmproject.R
import com.znevs.filmproject.domain.global.exceptions.NoNetworkException
import com.znevs.filmproject.domain.global.models.FilmDomainModel
import com.znevs.filmproject.domain.filmlist.FilmsInteractor
import com.znevs.filmproject.presentation.ui.filmdetail.FilmDetailScreenData
import com.znevs.filmproject.presentation.ui.global.base.BaseViewModel
import com.znevs.filmproject.presentation.ui.global.navigation.PlaneRouter
import com.znevs.filmproject.presentation.ui.global.navigation.Screens
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch
import javax.inject.Inject

sealed class FilmsViewState {
    object Default : FilmsViewState()
    data class Data(val data: List<FilmDomainModel>?) : FilmsViewState()
    object Loading : FilmsViewState()
    object LoadingSwipe : FilmsViewState()
    data class Error(val errorType: FilmsErrorType) : FilmsViewState()
}

sealed class FilmsViewEvent {
    object LoadFilms : FilmsViewEvent()
    object RefreshFilms : FilmsViewEvent()
    data class SaveFilm(val model: FilmDomainModel, val position: Int) : FilmsViewEvent()
    data class DeleteSavedFilm(val id: Long, val position: Int) : FilmsViewEvent()
    data class CheckFilmsIsSaved(val pairList: List<Pair<FilmDomainModel, Int>>) : FilmsViewEvent()
    data class ShowFilmDetail(val id: Long) : FilmsViewEvent()
}

sealed class FilmsErrorType {
    data class UnknownError(@StringRes val message: Int) : FilmsErrorType()
    object NoNetworkError : FilmsErrorType()
    object EmptyDataError : FilmsErrorType()
}

sealed class FilmsViewEffect {
    data class SaveFailed(val position: Int) : FilmsViewEffect()
    data class DeleteSaveFailed(val position: Int) : FilmsViewEffect()
    data class UpdateFilmsModel(val pairList: List<Pair<FilmDomainModel, Int>>) : FilmsViewEffect()
}

class FilmsViewModel @Inject constructor(
    private val interactor: FilmsInteractor,
    private val router: PlaneRouter
) : BaseViewModel<FilmsViewState, FilmsViewEvent, FilmsViewEffect>() {

    init {
        viewState = FilmsViewState.Default
    }

    override fun processEvent(viewEvent: FilmsViewEvent) {
        when (viewEvent) {
            is FilmsViewEvent.LoadFilms -> {
                if (viewState is FilmsViewState.Default) {
                    loadFilms()
                }
                if (viewState is FilmsViewState.Data) {
                    val data = (viewState as FilmsViewState.Data).data
                    if (data.isNullOrEmpty()) {
                        loadFilms()
                    }
                }
            }
            is FilmsViewEvent.RefreshFilms -> {
                loadFilms()
            }
            is FilmsViewEvent.SaveFilm -> {
                saveFilm(viewEvent.model, viewEvent.position)
            }
            is FilmsViewEvent.DeleteSavedFilm -> {
                deleteSavedFilm(viewEvent.id, viewEvent.position)
            }
            is FilmsViewEvent.CheckFilmsIsSaved -> {
                checkFilmsIsSaved(viewEvent.pairList)
            }
            is FilmsViewEvent.ShowFilmDetail -> {
                loadFilmDetail(viewEvent.id)
            }
        }
    }

    private fun loadFilms() {
        viewState = when (viewState) {
            is FilmsViewState.Data -> FilmsViewState.LoadingSwipe
            else -> FilmsViewState.Loading
        }

        viewModelScope.launch {
            viewState = try {
                val result = interactor.getFilms()

                if (result.isEmpty()) {
                    FilmsViewState.Error(FilmsErrorType.EmptyDataError)
                } else {
                    FilmsViewState.Data(result)
                }
            } catch (e: NoNetworkException) {
                FilmsViewState.Error(FilmsErrorType.NoNetworkError)
            } catch (e: Exception) {
                FilmsViewState.Error(FilmsErrorType.UnknownError(R.string.error_unknown))
            }
        }
    }

    private fun saveFilm(model: FilmDomainModel, position: Int) {
        viewModelScope.launch {
            val result = interactor.saveFilm(model)

            if (!result) {
                viewEffect = FilmsViewEffect.SaveFailed(position)
            }
        }
    }

    private fun deleteSavedFilm(id: Long, position: Int) {
        viewModelScope.launch {
            val result = interactor.deleteSaveFilm(id)

            if (!result) {
                viewEffect = FilmsViewEffect.DeleteSaveFailed(position)
            }
        }
    }

    private fun checkFilmsIsSaved(pairList: List<Pair<FilmDomainModel, Int>>) {
        viewModelScope.launch {
            val deferred = mutableListOf<Deferred<Pair<FilmDomainModel, Int>>>()

            for (pair in pairList) {
                deferred.add(async {
                    val isSaved = interactor.isFilmSaved(pair.first.filmId)
                    pair.first.copy(isSaved = isSaved) to pair.second
                })
            }

            val resultModelList = deferred.awaitAll()
            viewEffect = FilmsViewEffect.UpdateFilmsModel(resultModelList)
        }
    }

    private fun loadFilmDetail(id: Long) {
        viewModelScope.launch {
            val result = interactor.getFilmDetail(id)
            router.navigateTo(Screens.FilmDetailScreen(FilmDetailScreenData(result)))
        }
    }
}