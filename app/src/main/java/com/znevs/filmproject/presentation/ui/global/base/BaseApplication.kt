﻿package com.znevs.filmproject.presentation.ui.global.base

import dagger.android.DaggerApplication

abstract class BaseApplication : DaggerApplication()