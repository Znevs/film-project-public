﻿package com.znevs.filmproject.presentation.ui.flowmain

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.znevs.filmproject.R
import com.znevs.filmproject.presentation.ui.global.base.BaseFragment
import com.znevs.filmproject.presentation.ui.filmlist.FilmsFragment
import com.znevs.filmproject.presentation.ui.savedfilmlist.SavedFilmsFragment
import kotlinx.android.synthetic.main.flow_fragment_main.*


class MainFlowFragment : BaseFragment(R.layout.flow_fragment_main) {

    companion object {
        fun newInstance(): MainFlowFragment {
            return MainFlowFragment()
        }
    }

    private val tabNameList = listOf(R.string.title_films, R.string.title_saved_films)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vp2_main.adapter = FilmsViewPagerAdapter(childFragmentManager, viewLifecycleOwner.lifecycle)
        vp2_main.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        vp2_main.setPageTransformer(MarginPageTransformer(1500))

        TabLayoutMediator(tab_main, vp2_main) { tab, position ->
            tab.text = getString(tabNameList[position])
        }.attach()
    }

    class FilmsViewPagerAdapter(
        fragmentManager: FragmentManager,
        lifecycle: Lifecycle
    ) : FragmentStateAdapter(fragmentManager, lifecycle) {

        override fun getItemCount(): Int = 2

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> FilmsFragment()
                else -> SavedFilmsFragment()
            }
        }
    }
}