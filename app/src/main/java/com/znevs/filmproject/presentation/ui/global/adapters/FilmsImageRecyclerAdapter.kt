﻿package com.znevs.filmproject.presentation.ui.global.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.znevs.filmproject.R
import com.znevs.filmproject.presentation.utils.extension.addLoadError
import kotlinx.android.synthetic.main.item_film_image.view.*


class FilmsImageRecyclerAdapter : ListAdapter<String, FilmsImageRecyclerAdapter.ViewHolder>(FilmImageDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class FilmImageDiffCallback : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return newItem == oldItem
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return newItem == oldItem
        }
    }

    class ViewHolder private constructor(view: View) : RecyclerView.ViewHolder(view) {
        private val filmImage: ImageView = view.img_item_film

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater.inflate(R.layout.item_film_image, parent, false)

                return ViewHolder(view)
            }
        }

        fun bind(item: String) {
            filmImage.load(item) {
                addLoadError(filmImage)
            }
        }
    }
}