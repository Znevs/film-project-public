﻿package com.znevs.filmproject.presentation.ui.global.base

import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity()