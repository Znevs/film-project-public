﻿package com.znevs.filmproject.presentation.utils.extension

import android.os.Bundle
import androidx.fragment.app.Fragment

inline infix fun <T : Fragment> T.withArguments(closure: Bundle.() -> Unit): T {
    val bundle = Bundle()
    bundle.closure()
    arguments = bundle
    return this
}
