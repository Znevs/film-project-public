﻿package com.znevs.filmproject.presentation.ui.global.navigation

import androidx.fragment.app.Fragment
import com.znevs.filmproject.presentation.ui.flowmain.MainFlowFragment
import com.znevs.filmproject.presentation.ui.filmdetail.FilmDetailFragment
import com.znevs.filmproject.presentation.ui.filmdetail.FilmDetailScreenData
import ru.terrakok.cicerone.android.support.SupportAppScreen

class Screens {
    class MainFlowScreen() : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return MainFlowFragment.newInstance()
        }

        init {
            screenKey = javaClass.simpleName
        }
    }

    class FilmDetailScreen(private val data: FilmDetailScreenData) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return FilmDetailFragment.newInstance(data)
        }

        init {
            screenKey = javaClass.simpleName + "_" + data.film.filmId
        }
    }
}