﻿package com.znevs.filmproject.presentation.ui.savedfilmlist

import androidx.annotation.StringRes
import androidx.lifecycle.viewModelScope
import com.znevs.filmproject.R
import com.znevs.filmproject.domain.global.models.FilmDomainModel
import com.znevs.filmproject.domain.savedfilmlist.SavedFilmsInteractor
import com.znevs.filmproject.presentation.ui.filmdetail.FilmDetailScreenData
import com.znevs.filmproject.presentation.ui.global.base.BaseViewModel
import com.znevs.filmproject.presentation.ui.global.navigation.PlaneRouter
import com.znevs.filmproject.presentation.ui.global.navigation.Screens
import kotlinx.coroutines.launch
import javax.inject.Inject


sealed class SavedFilmsViewState {
    object Default : SavedFilmsViewState()
    data class Data(val data: List<FilmDomainModel>?) : SavedFilmsViewState()
    object Loading : SavedFilmsViewState()
    object LoadingSwipe : SavedFilmsViewState()
    data class Error(val errorType: SavedFilmsErrorType) : SavedFilmsViewState()
}

sealed class SavedFilmsViewEvent {
    object LoadFilms : SavedFilmsViewEvent()
    data class DeleteFromSavedFilm(val model: FilmDomainModel, val position: Int) : SavedFilmsViewEvent()
    data class ShowFilmDetail(val id: Long) : SavedFilmsViewEvent()
}

sealed class SavedFilmsViewEffect {
    data class DeleteFromSavedFailed(val model: FilmDomainModel, val position: Int) : SavedFilmsViewEffect()
}

sealed class SavedFilmsErrorType {
    data class UnknownError(@StringRes val message: Int) : SavedFilmsErrorType()
    object NotSavedFilmsError : SavedFilmsErrorType()
}

class SavedFilmsViewModel @Inject constructor(
    private val interactor: SavedFilmsInteractor,
    private val router: PlaneRouter
) : BaseViewModel<SavedFilmsViewState, SavedFilmsViewEvent, SavedFilmsViewEffect>() {

    init {
        viewState = SavedFilmsViewState.Default
    }

    override fun processEvent(viewEvent: SavedFilmsViewEvent) {
        when (viewEvent) {
            is SavedFilmsViewEvent.LoadFilms -> {
                loadFilms()
            }
            is SavedFilmsViewEvent.DeleteFromSavedFilm -> {
                deleteSavedFilm(viewEvent.model, viewEvent.position)
            }
            is SavedFilmsViewEvent.ShowFilmDetail -> {
                loadFilmDetail(viewEvent.id)
            }
        }
    }

    private fun loadFilms() {
        viewState = when (viewState) {
            is SavedFilmsViewState.Data -> SavedFilmsViewState.LoadingSwipe
            else -> SavedFilmsViewState.Loading
        }

        viewModelScope.launch {
            viewState = try {
                val result = interactor.getSavedFilms()
                getStateByData(result)
            } catch (e: Exception) {
                SavedFilmsViewState.Error(SavedFilmsErrorType.UnknownError(R.string.error_unknown))
            }
        }
    }

    private fun deleteSavedFilm(model: FilmDomainModel, position: Int) {
        viewModelScope.launch {
            val result = interactor.deleteSaveFilm(model.filmId)

            if (!result) {
                viewEffect = SavedFilmsViewEffect.DeleteFromSavedFailed(model, position)
            } else {
                if (viewState is SavedFilmsViewState.Data) {
                    (viewState as SavedFilmsViewState.Data).data?.toMutableList()?.let { data ->
                        data.removeAt(position)
                        viewState = getStateByData(data)
                    }
                }
            }
        }
    }

    private fun getStateByData(data: List<FilmDomainModel>): SavedFilmsViewState {
        return if (data.isEmpty()) {
            SavedFilmsViewState.Error(SavedFilmsErrorType.NotSavedFilmsError)
        } else {
            SavedFilmsViewState.Data(data)
        }
    }

    private fun loadFilmDetail(id: Long) {
        viewModelScope.launch {
            try {
                val result = interactor.getFilmDetail(id)
                router.navigateTo(Screens.FilmDetailScreen(FilmDetailScreenData(result)))
            } catch (e: Exception) {
            }
        }
    }
}